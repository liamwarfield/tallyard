# tallyard

tallyard is an authenticated voting system that ensures voter privacy while
preventing double-voting.  Voter privacy is achieved via the homomorphic secret
sharing system outlined
[here](https://en.wikipedia.org/wiki/Homomorphic_secret_sharing#Example:_decentralized_voting_protocol)
and double-voting is prevented by a STARKs zero-knowledge proof as described
[here](https://vitalik.ca/general/2017/11/09/starks_part_1.html).  Voters'
computers communicate with each other directly over a peer-to-peer network.
While there is a "master" node that decides what the candidates of the election
are, it does not have any privileges once voting begins.

Have a look at the
[presentation](https://gitlab.com/edwargix/tallyard/-/raw/master/doc/oresec-talk/tallyard.pdf)
I gave for my university's cybersecurity club for an overview of the
mathematics.

The zero-knowledge implementation is not entirely finished.

tallyard is very much a work-in-progress and is only useful for elections where
everyone is voting simultaneously, such as a club election, [which it was
actually used
for](https://mailman.mines.edu/pipermail/lug/2020-April/000573.html).

# Installation

## Arch Linux

```sh
yay -S tallyard
```

## From Source

Ensure you have the [Go](https://golang.org) programming language
installed.  Then:

```sh
$ git clone https://gitlab.com/edwargix/tallyard
$ cd tallyard
$ make
$ # the tallyard binary is now at ./tallyard
```

# Usage

Once every voter wishing to participate in the election has tallyard installed,
everyone should run it:

```sh
$ tallyard
```

One voter, henceforth called "the master", will then choose "Create Election",
enter the candidates of the election, and share the printed "election key" with
everyone else[^1].  Every voter who is not the master will instead select "Join
Election" and paste the "election key" provided by the master.  Voting will
begin once the master hits the enter key.  However, the master should only do so
once he has received confirmation of a connection from all of the other voters.

[^1]: preferably over a secure medium

# Why Go?

- concurrency
- [libp2p](https://libp2p.io)

# Future Work

- Support authentication systems.  First on the list is
  [Matrix](https://matrix.org/) accounts.  Those who used the old client written
  in Racket will know that authentication used to be possible; the transition
  just hasn't been made yet.
- [Matrix](https://matrix.org) bots for voting on topics in matrix chat rooms.
- Finish the zero-knowledge proof implementation.

# Origin of the name

"Tillyard" is the last name of the author of the *The Elizabethan World
Picture*, which I read in my phenomenal Shakespeare class.  Also, the voting
system works by keeping a "tally" of votes in a secretive manner.  These two
notions together yield "tallyard".

"tallyard" is written in lowercase letters to follow the convention of UNIX
executable names being all lowercase.
