package main

import (
	"math/big"
)

type Matrix [][]big.Rat

func (M Matrix) Rows() int {
	return len(M)
}

func (M Matrix) Cols() int {
	if len(M) == 0 {
		return 0
	}
	return len(M[0])
}

func (M Matrix) RREF() {
	lead := 0
	zero := big.NewRat(0, 1)

	for r := 0; r < M.Rows(); r++ {
		if lead >= M.Cols() {
			return
		}
		i := r
		for M[i][lead].Cmp(zero) == 0 {
			i++
			if M.Rows() == i {
				i = r
				lead++
				if M.Cols() == lead {
					return
				}
			}
		}
		M[i], M[r] = M[r], M[i]
		f := &big.Rat{}
		f.Inv(&M[r][lead])
		for j := range M[r] {
			M[r][j].Mul(&M[r][j], f)
		}
		for i = 0; i < M.Rows(); i++ {
			if i != r {
				f.Set(&M[i][lead])
				for j, e := range M[r] {
					M[i][j].Sub(&M[i][j], new(big.Rat).Mul(&e, f))
				}
			}
		}
		lead++
	}
}

// assumes `M' and `other' are valid matrices
func (M Matrix) Equals(other Matrix) bool {
	if len(M) != len(other) {
		return false
	}
	if len(M) > 0 && len(M[0]) != len(other[0]) {
		return false
	}
	for r := range M {
		for c := range M[r] {
			if M[r][c].Cmp(&other[r][c]) != 0 {
				return false
			}
		}
	}
	return true
}
