package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"io"
	"os"
	"sync"

	"github.com/cbergoon/merkletree"
	"github.com/ipfs/go-log"
	"github.com/libp2p/go-libp2p"
	"github.com/libp2p/go-libp2p-core/host"
	"github.com/libp2p/go-libp2p-core/peer"
	"github.com/libp2p/go-libp2p-core/protocol"
	dht "github.com/libp2p/go-libp2p-kad-dht"
	routing "github.com/libp2p/go-libp2p-routing"
	"github.com/mr-tron/base58/base58"
	"github.com/rivo/tview"
	"github.com/whyrusleeping/go-logging"
)

var (
	logger          = log.Logger("tallyard")
	protocolID      = protocol.ID("/tallyard/0.0.0")
	candidates      []Candidate
	optionsMerkle   *merkletree.MerkleTree
	rendezvousNonce Nonce
	merkleRoot      []byte
	me              Me
	election        Election
)

func bootstrap() {
	var err error

	me.ctx = context.Background()
	election.remoteVoters = make(map[peer.ID]*Voter)

	me.Host, err = libp2p.New(me.ctx,
		libp2p.Routing(func(h host.Host) (routing.PeerRouting, error) {
			var err error
			me.kdht, err = dht.New(me.ctx, h)
			if err != nil {
				return me.kdht, err
			}
			logger.Info("boostrapping the DHT")
			if err = me.kdht.Bootstrap(me.ctx); err != nil {
				panic(err)
			}
			return me.kdht, err
		}),
	)
	if err != nil {
		panic(err)
	}

	logger.Info("host:", me.ID())
	logger.Info(me.Addrs())

	var wg sync.WaitGroup
	for _, peerAddr := range dht.DefaultBootstrapPeers {
		peerInfo, _ := peer.AddrInfoFromP2pAddr(peerAddr)
		wg.Add(1)
		go func() {
			defer wg.Done()
			if err := me.Connect(me.ctx, *peerInfo); err != nil {
				logger.Warning(err)
			} else {
				logger.Info("connection established with bootstrap node:", *peerInfo)
			}
		}()
	}
	wg.Wait()

	if election.masterID == "" { // we are the master
		fmt.Println("share this with peers:")
		fmt.Printf("%s0%s\n",
			base58.Encode(optionsMerkle.MerkleRoot()),
			me.ID())

		logger.Info("waiting for incoming streams and finding voters...")

		election.Lock()
		ch := make(chan int, 1)
		election.close = ch
		go findPeers(ch)
		election.Unlock()

		me.SetStreamHandler(protocolID, streamHandler)

		fmt.Println("press ENTER to solidify group of voters and start voting")
		stdReader := bufio.NewReader(os.Stdin)
		_, err := stdReader.ReadString('\n')
		if err != nil {
			panic(err)
		}

		logger.Info("ENTER has been pressed; closing election")
		election.Lock()
		n := len(election.remoteVoters)
		election.close <- n
		close(election.close)
		election.Unlock()
		election.RLock()
		for _, voter := range election.remoteVoters {
			stream, err := me.NewStream(me.ctx, voter.addrInfo.ID, protocolID)
			if err != nil {
				panic(err)
			}
			writer := bufio.NewWriter(stream)
			writer.WriteString(fmt.Sprintf("close\n%d", n))
			writer.Flush()
			stream.Close()
		}
		election.RUnlock()
	} else { // we are a slave
		logger.Info("attempting to open stream with master peer...")
		stream, err := me.NewStream(me.ctx, election.masterID, protocolID)
		rw := bufio.NewReadWriter(bufio.NewReader(stream), bufio.NewWriter(stream))
		if err != nil {
			panic(err)
		}
		logger.Info("opened stream with master peer")

		logger.Info("fetching election info from master")
		_, err = rw.WriteString("info")
		if err != nil {
			panic(err)
		}
		rw.Flush()
		stream.Close() // only stops writing
		// first field is the rendezvous string, which is used for peer
		// discovery
		str, err := rw.ReadString('\n')
		if err != nil && err != io.EOF {
			panic(err)
		}
		str = stripNewline(str)
		rendezvousNonce = Nonce(str)
		// remaining fields are the candidates
		for {
			str, err := rw.ReadString('\n')
			if err != nil && err != io.EOF {
				panic(err)
			}
			str = stripNewline(str)
			if str != "" {
				candidates = append(candidates, Candidate(str))
			}
			if err == io.EOF {
				break
			}
		}
		logger.Info("done fetching election info")

		logger.Info("checking authenticity of election info...")
		verifyElectionInfo()

		// channel used to signify when election is closed
		ch := make(chan int, 1)
		election.close = ch
		// now that we have election info, begin handling streams
		me.SetStreamHandler(protocolID, streamHandler)
		findPeers(ch)
	}
}

func main() {
	log.SetAllLoggers(logging.WARNING)

	debug := flag.Bool("d", false, "enable extra logging for debugging")
	flag.Parse()
	if *debug {
		log.SetLogLevel("tallyard", "info")
	} else {
		log.SetLogLevel("dht", "critical")
		log.SetLogLevel("relay", "critical")
		log.SetLogLevel("tallyard", "critical")
	}

	app := tview.NewApplication()
	modal := tview.NewModal().
		SetText("Welcome to tallyard!").
		AddButtons([]string{"Create Election", "Join Election"}).
		SetDoneFunc(func(buttonIndex int, buttonLabel string) {
			app.Stop()
			switch buttonLabel {
			case "Create Election":
				createElection()
			case "Join Election":
				joinElection()
			}
		})
	if err := app.SetRoot(modal, false).EnableMouse(true).Run(); err != nil {
		panic(err)
	}

	bootstrap()

	startVoting()
}
