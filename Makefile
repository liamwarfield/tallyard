GOSRC != find . -name '*.go'
GOSRC += go.mod go.sum
GO = go
RM ?= rm -f

tallyard: $(GOSRC)
	$(GO) build -o $@
