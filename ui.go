package main

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
	"unicode"

	"github.com/cbergoon/merkletree"
	"github.com/libp2p/go-libp2p-core/peer"
	"github.com/mr-tron/base58/base58"
	"github.com/rivo/tview"
)

func createElection() {
	var form *tview.Form
	n := 3
	app := tview.NewApplication()
	form = tview.NewForm().
		AddInputField("1.", "", 50, nil, nil).
		AddInputField("2.", "", 50, nil, nil).
		AddButton("+", func() {
			form.AddInputField(fmt.Sprintf("%d.", n), "", 50, nil, nil)
			n++
		}).
		AddButton("-", func() {
			// TODO: ensure from joiner that there are at least two
			// candidates
			if n > 3 {
				form.RemoveFormItem(n - 2)
				n--
			}
		}).
		AddButton("Done", func() {
			// TODO: ensure none of the candidates are empty
			app.Stop()
			rendezvousNonce = NewNonce()
			content := []merkletree.Content{rendezvousNonce}
			for i := 0; i < n-1; i++ {
				eo := Candidate(form.GetFormItem(i).(*tview.InputField).GetText())
				candidates = append(candidates, eo)
				content = append(content, eo)
			}
			var err error
			optionsMerkle, err = merkletree.NewTree(content)
			if err != nil {
				panic(err)
			}
		})
	if err := app.SetRoot(form, true).EnableMouse(true).Run(); err != nil {
		panic(err)
	}
}

func joinElection() {
	app := tview.NewApplication()
	var form *tview.Form
	form = tview.NewForm().
		AddInputField("Election key:", "", 100, nil, nil).
		AddButton("Continue", func() {
			app.Stop()
			electionKey := form.GetFormItem(0).(*tview.InputField).GetText()

			zeroi := strings.IndexByte(electionKey, '0')
			var err error
			logger.Info("merkle root:", electionKey[:zeroi])
			merkleRoot, err = base58.Decode(electionKey[:zeroi])
			if err != nil {
				panic(err)
			}

			election.masterID, err = peer.Decode(electionKey[zeroi+1:])
			if err != nil {
				panic(err)
			}
			logger.Info("master ID:", election.masterID)
		})
	if err := app.SetRoot(form, true).EnableMouse(true).Run(); err != nil {
		panic(err)
	}
}

// displays a voting UI to the user and returns the encoded ballot
func vote(candidates []Candidate) []byte {
	ranks := make([]int, len(candidates))
	app := tview.NewApplication()
	form := tview.NewForm()

	for _, eo := range candidates {
		// TODO: support more than 99 candidates
		form.AddInputField(string(eo), "", 2,
			func(textToCheck string, lastChar rune) bool {
				return len(textToCheck) < 3 && unicode.IsDigit(lastChar)
			}, nil)
	}

	form.AddButton("Submit", func() {
		app.Stop()
		for i := 0; i < len(candidates); i++ {
			rank, err := strconv.Atoi(form.GetFormItem(i).(*tview.InputField).GetText())
			if err != nil {
				panic(err)
			}
			ranks[i] = rank
		}
	})

	if err := app.SetRoot(form, true).EnableMouse(true).Run(); err != nil {
		panic(err)
	}

	return GetBallotFromRankings(ranks)
}

func GetBallotFromRankings(ranks []int) []byte {
	n := len(ranks)
	candidates := make([]int, n)

	for i := 0; i < n; i++ {
		candidates[i] = i
	}

	// sort candidates by ranking
	cr := CandidateRanking{candidates, ranks}
	sort.Sort(&cr)

	// TODO: support more than 255 voters (limit from usage of byte)
	prefMatrix := make([][]byte, n)

	for len(candidates) > 0 {
		r := ranks[candidates[0]]
		i := 1
		for i < len(candidates) && ranks[candidates[i]] == r {
			i++
		}
		// i is now index of the first candidate with worse (i.e. higher
		// in value) rank
		row := make([]byte, n)
		for j := i; j < len(candidates); j++ {
			row[candidates[j]] = 1
		}
		for j := 0; j < i; j++ {
			prefMatrix[candidates[j]] = row
		}
		candidates = candidates[i:]
	}

	// convert 2D array into 1D array
	barray := make([]byte, 0, n*n)
	for _, row := range prefMatrix {
		barray = append(barray, row...)
	}

	return barray
}

type CandidateRanking struct {
	candidates []int // becomes list of candidate IDs sorted by rank
	ranks      []int // maps candidate ID to rank
}

func (cr *CandidateRanking) Len() int {
	return len(cr.ranks)
}

func (cr *CandidateRanking) Less(i, j int) bool {
	return cr.ranks[cr.candidates[i]] < cr.ranks[cr.candidates[j]]
}

func (cr *CandidateRanking) Swap(i, j int) {
	tmp := cr.candidates[i]
	cr.candidates[i] = cr.candidates[j]
	cr.candidates[j] = tmp
}
